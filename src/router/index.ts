import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Root',
    component: () => import("@/views/Root/Root.vue"),
      children: [
          {
              path: "/",
              name: "Home",
              meta: {
                  title: "首页"
              },
              component: () => import("@/views/Root/Home/Home.vue"),
          },
          {
              path: "topics",
              name: "Topics",
              meta: {
                  title: "话题"
              },
              component: () => import("@/views/Root/Topics/Topics.vue")
          },
          {
              path: "questions",
              name: "Questions",
              meta: {
                  title: "问答"
              },
              component: () => import("@/views/Root/Questions/Questions.vue")
          },
          {
              path: "articles",
              name: "Articles",
              meta: {
                  title: "文章"
              },
              component: () => import("@/views/Root/Articles/Articles.vue")
          },
          {
              path: "users",
              name: "Users",
              meta: {
                  title: "人脉"
              },
              component: () => import("@/views/Root/Users/Users.vue")
          }
      ]
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
