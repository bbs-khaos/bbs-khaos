import axios from "axios";
import qs from "qs";

const http = axios.create({
    baseURL: "",
    timeout: 10000
});

http.interceptors.request.use(function(config){
    config.data = qs.stringify(config.data);
    return config;
});

http.interceptors.response.use(function(response){
    return response;
}, function(error){
    return Promise.reject(error);
});
