import { createStore } from 'vuex';
import VuexPersistence from "vuex-persist";

export default createStore({
  state: {
      active: "1",
      subTitle: false,
  },
  mutations: {
      toggleActive(state, payload){
          state.active = payload;
      },
      toggleSubTitle(state, payload){
          state.subTitle = payload;
      }
  },
  actions: {
  },
  modules: {
  },
    plugins: [
        new VuexPersistence({ storage: window.sessionStorage }).plugin
    ]
})
